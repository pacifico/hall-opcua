
import sys
import can
sys.path.insert(0, "..")
import time
import datetime
import opcua
from CalibCalc import CalibCalc

calEngine = CalibCalc.CalibCalc()

from opcua import ua, Server

serverStartTime = datetime.datetime.now()

if __name__ == "__main__":
	# setup our server
    server = Server()
    server.set_endpoint("opc.tcp://0.0.0.0:4840/freeopcua/server/")

    # setup our own namespace, not really necessary but should as spec
    uri = "na62magmonitor.cern.ch"
    idx = server.register_namespace(uri)
    objects = server.get_objects_node()

    objArray = []
    
    objArray.append(objects.add_object(idx, "Downstream"))
    objArray.append(objects.add_object(idx, "Center"))
    objArray.append(objects.add_object(idx, "Upstream"))
    
    varMatrix = []
    
    for obj in objArray:
        varMatrix.append(list())
        varMatrix[-1].append(obj.add_variable(idx, "ADCBx", 0))
        varMatrix[-1].append(obj.add_variable(idx, "ADCBy", 0))
        varMatrix[-1].append(obj.add_variable(idx, "ADCBz", 0))
        varMatrix[-1].append(obj.add_variable(idx, "ADCTemp", 0))
        varMatrix[-1].append(obj.add_variable(idx, "Bx", 0.0))
        varMatrix[-1].append(obj.add_variable(idx, "By", 0.0))
        varMatrix[-1].append(obj.add_variable(idx, "Bz", 0.0))
        varMatrix[-1].append(obj.add_variable(idx, "Temperature", 0.0))
        varMatrix[-1].append(obj.add_variable(idx, "Address", 0))
    
    objArray.append(objects.add_object(idx, "UptimeCounter"))
    varTime = objArray[-1].add_variable(idx, "seconds", 0);

    
    
    server.start()
    
    try:
        count = 0
        while True:
            time.sleep(1)
            count += 0.1
            varTime.set_value((datetime.datetime.now()-serverStartTime).total_seconds())
    finally:
        #close connection, remove subcsriptions, etc
        server.stop()
    
